var Bicicleta = function(id, color, modelo, ubicacion){
    this.id=id;
    this.color= color;
    this.modelo= modelo;
    this.ubicacion= ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis=[];
Bicicleta.add= function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById= function(aBiciId){
    var aBici= Bicicleta.allBicis.find(x=>x.id == aBiciId);
    if(aBici)
       return aBici;
}

Bicicleta.removeById = function(aBiciId){
    for (let index = 0; index < Bicicleta.allBicis.length; index++) {
        if(Bicicleta.allBicis[index].id==aBiciId){
            Bicicleta.allBicis.splice(index,1);
            break;
        }

    }
}

var b = new Bicicleta(1,'negro','urbano',[-6.701,-79.90654]);

Bicicleta.add(b);
module.exports=Bicicleta;
