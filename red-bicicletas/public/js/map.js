var mymap = L.map('mapid',{scrollWheelZoom:false}).setView([-6.7034434,-79.9046852], 16);//que ubique el mapa en esa coordenada


L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);



var biciMarker = L.icon({
    iconUrl: 'assets/img/bici.png',
    iconSize:     [38,38 ]
});


L.marker([-6.70360,-79.90622]).addTo(mymap).bindPopup("Parroquia San Pedro").openPopup();
L.marker([-6.70253,-79.90191]).addTo(mymap).bindPopup("Hospital Belén");
L.marker([-6.70569,-79.90240]).addTo(mymap).bindPopup("Mercado");
L.marker([-6.70441,-79.90264]).addTo(mymap).bindPopup("Farmacia");
L.marker([-6.70655,-79.90618]).addTo(mymap).bindPopup("Zoológico universitario");
L.marker([-6.70753,-79.90698]).addTo(mymap).bindPopup("Universidad");


$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result){
    console.log(result)
    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion, {title: bici.id   ,icon:biciMarker  }).addTo(mymap);
    })
  }
})
