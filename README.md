# Proyecto
Proyecto realizado durante el  curso "Desarrollo del lado del servidor:  NodeJS, Express y MongoDB"

Hasta el momento tenemos un pequeño CRUD en memoria y una API sencilla (semana1)

![texto](./fotos/foto.png)
## Prerrequisitos
Todo lo que necesitas para tener una copia de este proyecto en tu computadora ♥.
#### Tener Node.js instalado: https://nodejs.org/es/download/
#### Tener Git instalado: https://git-scm.com/downloads

![texto](./fotos/necesario.png)
## Pasos para la instalación
Ejecutar los siguientes comandos
```
git clone https://Danielsinnn@bitbucket.org/Danielsinnn/node-practicas.git
```
```
cd red-bicicletas
```
```
npm install
```
```
npm i nodemon --save -dev
```
## Para Ejecutar
```
npm run devstart
```
## Autor

* **Daniel Ramírez Huayanay** - [Danielsinnn](https://bitbucket.org/Danielsinnn/) 
